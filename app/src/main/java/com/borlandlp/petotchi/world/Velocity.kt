package com.borlandlp.petotchi.world

class Velocity (
    var x: Float,
    var y: Float
) {
    fun add(velocity: Velocity) {
        x += velocity.x
        y += velocity.y
    }

    fun multiple(velocity: Velocity) {
        x *= velocity.x
        y *= velocity.y
    }

    fun divide(velocity: Velocity) {
        x /= velocity.x
        y /= velocity.y
    }

    fun reduce(velocity: Velocity) {
        x -= velocity.x
        y -= velocity.y
    }
}

class RawDirection(
    var x: Float,
    var y: Float
)