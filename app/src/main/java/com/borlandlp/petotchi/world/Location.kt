package com.borlandlp.petotchi.world

class Location (val x: Float = 0F, val y: Float = 0F) {
    override fun toString(): String {
        return "Location {x: ${x}, y:${y}}"
    }

    fun isEqual(other: Location): Boolean {
        return other.x == this.x && other.y == this.y
    }
}