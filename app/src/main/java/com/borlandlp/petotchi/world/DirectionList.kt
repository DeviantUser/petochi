package com.borlandlp.petotchi.world

enum class DirectionList(val type: DirectionType) {
    LEFT(DirectionType.HORIZONTAL),
    RIGHT(DirectionType.HORIZONTAL),
    DOWN(DirectionType.VERTICAL),
    UP(DirectionType.VERTICAL),
    NONE(DirectionType.NONE);

    companion object {
        fun getRawDirection(hDir: DirectionList, vDir: DirectionList): RawDirection {
            return RawDirection(
                x = when (hDir) {
                    LEFT -> {
                        -1F
                    }
                    RIGHT -> {
                        1F
                    }
                    else -> {
                        0F
                    }
                },
                y = when (vDir) {
                    UP -> {
                        -1F
                    }
                    DOWN -> {
                        1F
                    }
                    else -> {
                        0F
                    }
                }
            )
        }

        fun rawDirectionToXDirection(raw: RawDirection): DirectionList {
            return when {
                raw.x < 0 -> LEFT
                raw.x > 0 -> RIGHT
                else -> NONE
            }
        }

        fun rawDirectionToYDirection(raw: RawDirection): DirectionList {
            return when {
                raw.y < 0 -> UP
                raw.y > 0 -> DOWN
                else -> NONE
            }
        }

        fun getOppositeDirection(dir: DirectionList): DirectionList {
            return when (dir) {
                LEFT -> RIGHT
                RIGHT -> LEFT
                UP -> DOWN
                DOWN -> UP
                else -> NONE
            }
        }
    }
}

enum class DirectionType {
    VERTICAL,
    HORIZONTAL,
    NONE,
}