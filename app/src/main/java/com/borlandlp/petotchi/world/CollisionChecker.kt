package com.borlandlp.petotchi.world

import com.borlandlp.petotchi.entity.Entity

class CollisionChecker {
//    /**
//     * @param fromEntity Энтити, которая хочет проверить коллизию с кем-то
//     * @param forEntity Энтити, с которой проверка на коллизицию
//     */
//    fun isCollide(fromEntity: Entity, forEntity: Entity, checkVelocity: Boolean = false): Boolean {
//        return fromEntity.location.x + fromEntity.velocity.x < forEntity.location.x + forEntity.velocity.x + forEntity.width && /* left */
//                fromEntity.location.x + fromEntity.width + fromEntity.velocity.x > forEntity.location.x + forEntity.velocity.x && /* right */
//                fromEntity.location.y + fromEntity.velocity.y < forEntity.location.y + forEntity.velocity.y + forEntity.height && /* up */
//                fromEntity.location.y + fromEntity.velocity.y + fromEntity.height > forEntity.location.y + forEntity.velocity.y /* bottom */
//    }

    /**
     * @param forEntity Энтити, которая хочет проверить коллизию с кем-то
     * @param fromEntity Энтити, с которой проверка на коллизицию
     * @param fromEntityOffset Смещение координат, если сущность движется и нужно проверить возможность движения в следующую точку
     */
    fun isCollide(forEntity: Entity, fromEntity: Entity, fromEntityOffset: Offset = Offset(0F, 0F,0F,0F)): Boolean {
        return forEntity.location.x < fromEntity.location.x + fromEntity.width + fromEntityOffset.right && /* right */
                forEntity.location.x + forEntity.width + forEntity.velocity.x > fromEntity.location.x - fromEntityOffset.left && /* left */
                forEntity.location.y < fromEntity.location.y + fromEntity.height + fromEntityOffset.bottom && /* bottom */
                forEntity.location.y + forEntity.height > fromEntity.location.y - fromEntityOffset.top /* top */
    }

    fun isCollide(b1: Entity.BoundingBox, b2: Entity.BoundingBox): Boolean {
        return b1.left < b2.right && /* left */
                b1.right > b2.left && /* right */
                b1.top < b2.bottom && /* up */
                b1.bottom > b2.top /* bottom */
    }
}

data class Offset (
    val left: Float,
    val top: Float,
    val right: Float,
    val bottom: Float
)