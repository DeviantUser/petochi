package com.borlandlp.petotchi.world

import com.borlandlp.petotchi.entity.*
import com.borlandlp.petotchi.utils.MathUtils
import kotlin.math.abs
import kotlin.math.min

class World(
    val entityController: EntityController,
    var leftEdge: Int,
    var rightEdge: Int,
    var topEdge: Int,
    var bottomEdge: Int,
    private val collisionChecker: CollisionChecker
) {
    var gravityDirection = Velocity(x = 0F, y = -1F)
    var gravity = 0.5F
    var maximumSpeedOfFreeFall = 9.8F

    fun getNearbyEntities(e: Entity, distance: Int, filter: Class<out Entity>? = null) : List<Entity> {
        return entityController.getEntities().filter {
            it != e
                    && (filter == null || filter == it.javaClass)
                    && (distance == -1 || abs(it.getCenterLocation().x - e.getCenterLocation().x) <= distance)
                    && (distance == -1 || abs(it.getCenterLocation().y - e.getCenterLocation().y) <= distance)
        }
    }

    fun getNearbyEntities(x: Int, y: Int, distance: Int, filter: Class<out Entity>? = null) : List<Entity> {
        return entityController.getEntities().filter {
             (filter == null || filter == it.javaClass)
                && (distance == -1 || abs(it.getCenterLocation().x - x) <= distance)
                && (distance == -1 || abs(it.getCenterLocation().y - y) <= distance)
        }
    }

    fun getXDirectionFromEntityToEntity(fromEntity: Entity, targetEntity: Entity): DirectionList {
        val angle = MathUtils.calcRotationAngleInDegrees(centerLoc = fromEntity.getCenterLocation(), targetLoc = targetEntity.getCenterLocation())
        return when {
            angle >= 0 && angle <= 180 -> {
                DirectionList.RIGHT
            }
            else -> {
                DirectionList.LEFT
            }
        }
    }

    fun getYDirectionFromEntityToEntity(fromEntity: Entity, targetEntity: Entity): DirectionList {
        val angle = MathUtils.calcRotationAngleInDegrees(centerLoc = fromEntity.location, targetLoc = targetEntity.location)
        return when {
            angle >= 90 && angle <= 270 -> {
                DirectionList.DOWN
            }
            else -> {
                DirectionList.UP
            }
        }
    }

    fun getEntityByLocation(loc: Location): Entity? {
        return null
    }

    /**
     * @TODO Возвращать сторону, в которой произошла коллизия
     * @param from Энтити, которая хочет проверить коллизию с кем-то
     * @param to Энтити, с которой проверка на коллизицию
     */
    fun checkCollision(from: Entity, to: Entity, fromEntityOffset: Offset = Offset(0F, 0F,0F,0F)): Boolean {
        return collisionChecker.isCollide(
            fromEntity = from,
            forEntity = to,
            fromEntityOffset = fromEntityOffset
        )
    }


    fun isCollide(b1: Entity.BoundingBox, b2: Entity.BoundingBox): Boolean {
        return collisionChecker.isCollide(b1 = b1, b2 = b2)
    }

    /**
     * -1 - до конца карты
     * Испускает луч из центра энтити, который имеет дальность и ширину луча. Возвращает все сущности, попавшие в луч
     */
    fun raycast(
        entity: Entity,
        rawDirection: RawDirection,
        range: Float = -1F,
        beamWidth: Int = 100)
    : List<Entity> {
        val foundEntities = mutableListOf<Entity>()
        var curX = entity.getCenterLocation().x
        var curY = entity.getCenterLocation().y
        val beamPointSize = beamWidth
        val stepDist = beamPointSize
        val xVec = when {
            rawDirection.x > 0 -> {
                min(1F, rawDirection.x)
            }
            rawDirection.x < 0 -> {
                min(-1F, rawDirection.x)
            }
            else -> 0F
        }
        val yVec = when {
            rawDirection.y > 0 -> {
                min(1F, rawDirection.y)
            }
            rawDirection.y < 0 -> {
                min(-1F, rawDirection.y)
            }
            else -> 0F
        }
        var step = 1F
        do {
            foundEntities.addAll(
                entityController.getEntities().filter {
                    it !== entity
                            && collisionChecker.isCollide(
                        it.getBoundingBox(),
                        Entity.BoundingBox(
                            left = curX - (stepDist / 2),
                            right = curX + (stepDist / 2),
                            bottom = curY + (stepDist / 2),
                            top = curY - (stepDist / 2)
                        )
                    )
                }
            )
            step++
            curX += (xVec * stepDist).toInt()
            curY += (yVec * stepDist).toInt()
        } while (
            curX <= rightEdge
            && curX >= leftEdge
            && curY <= bottomEdge
            && curY >= topEdge
            && (range == -1F || step * stepDist <= range)
        )

        return foundEntities
    }

    fun entityHaveCollisionForDirection(entity: Entity, direction: DirectionList, applyVelocity: Boolean = true): Entity? {
        // Ищем сущности на пути следования
        val nearbyEntities = if (direction.type == DirectionType.HORIZONTAL) {
            raycast(entity = entity, rawDirection = RawDirection(0F, 0F).apply {
                x = if (direction == DirectionList.LEFT) -1F else 1F
            }, beamWidth = entity.height - 1) // TODO Поправить дальность луча
        } else {
            raycast(entity = entity, rawDirection = RawDirection(0F, 0F).apply {
                y = if (direction == DirectionList.UP) -1F else 1F
            }, beamWidth = entity.width - 1) // TODO Поправить дальность луча
        }

        // Проверяем сущности на коллизию
        val leftOffset = (if (direction == DirectionList.LEFT) 1F else 0F) - (if (applyVelocity) entity.velocity.x else 0F)
        val rightOffset = (if (direction == DirectionList.RIGHT) 1F else 0F) + (if (applyVelocity) entity.velocity.x else 0F)
        val topOffset = (if (direction == DirectionList.UP) 1F else 0F) - (if (applyVelocity) entity.velocity.y else 0F)
        val bottomOffset = (if (direction == DirectionList.DOWN) 1F else 0F) + (if (applyVelocity) entity.velocity.y else 0F)
        val collideDetect = nearbyEntities.filter {  worldEntity ->
            checkCollision(from = entity, to = worldEntity, fromEntityOffset = Offset(
                    left = leftOffset,
                    right = rightOffset,
                    top = topOffset,
                    bottom = bottomOffset
                )
            )
        }

        // Проверяем, что есть коллизия с сущностью в искомой нами стороне
        return if (direction.type == DirectionType.HORIZONTAL) {
            collideDetect.firstOrNull { getXDirectionFromEntityToEntity(fromEntity = entity, targetEntity = it) == direction }
        } else {
            collideDetect.firstOrNull { getYDirectionFromEntityToEntity(fromEntity = entity, targetEntity = it) == direction }
        }
    }

    /**
     * @param from От какой
     * @param to к какой
     */
    fun getOffsetBetweenTwoEntities(from: Entity, to: Entity): Entity.BoundingBox {
        return if (from.getCenterLocation().x < to.getCenterLocation().x) {
            Entity.BoundingBox(
                left = 0F,
                right = (to.getBoundingBox().left - 0.5F) - from.getBoundingBox().right,
                top = 0F,
                bottom = 0F
            )
        } else {
            Entity.BoundingBox(
                left = (to.getBoundingBox().right + 0.5F) - from.getBoundingBox().left,
                right = 0F,
                top = 0F,
                bottom = 0F
            )
        }
    }

    fun update() {
        entityController.update()
    }

    fun onDestroy() {

    }
}