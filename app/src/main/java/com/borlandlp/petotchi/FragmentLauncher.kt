package com.borlandlp.petotchi

import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

/**
 * Created by Alexander Efremenkov.
 * Date: 26.01.2015, 10:22.
 * In Intellij IDEA 14 Ultimate
 * aefremenkov@livamster.ru
 */
class FragmentLauncher(owner: MainActivity, var instance: Bundle?, private var listener: Listener?) {
    private val fragmentManager: FragmentManager = owner.supportFragmentManager
    private var linkFragment: Fragment? = null
    private val lastTransaction = Transaction()
    private val handler = Handler()
    private val container: Int = R.id.content_container
    private val lock = Any()

    interface Listener {
        fun onNewScreenOpened(fragmentCallback: NamedFragmentCallback?)
    }

    private val currentFragmentCallback: NamedFragmentCallback?
        private get() {
            synchronized(lock) { return fragmentManager.findFragmentById(container) as NamedFragmentCallback? }
        }

    fun showFragmentForce(fragment: Fragment) {
        if (!canRestoreFragment(fragment.javaClass.name)) {
            popBackStackImmediate()
            openFragmentForce(fragment, false)
        }
    }

    private fun canRestoreFragment(className: String): Boolean {
        return (popBackStackImmediate(className, 0)
                || popBackStackImmediate(className + "_force", 0))
    }

    private fun popBackStackImmediate(): Boolean {
        return fragmentManager.popBackStackImmediate()
    }

    private fun popBackStackImmediate(className: String, flags: Int): Boolean {
        return popBackStackImmediate(fragmentManager, className, flags)
    }

    fun openFragmentWithDelay(fragment: Fragment) {
        handler.postDelayed({ openFragment(fragment) }, FRAGMENT_OPEN_THRESHOLD.toLong())
    }

    fun openFragmentWithDelayPopAllStack(fragment: Fragment) {
        handler.postDelayed({ openFragment(fragment, true) }, FRAGMENT_OPEN_THRESHOLD.toLong())
    }

    fun openFragmentForceWithDelay(fragment: Fragment?, delay: Int) {
        handler.postDelayed({ openFragmentForce(fragment) }, delay.toLong())
    }

    fun openDialogFragmentWithDelay(dialogFragment: DialogFragment) {
        handler.postDelayed({ openDialogFragment(dialogFragment) }, FRAGMENT_OPEN_THRESHOLD
                .toLong())
    }

    @JvmOverloads
    fun openFragment(fragment: Fragment, popAllStack: Boolean = false) {
        if (transitionAllowed()) {
            synchronized(lock) {
                try {
                    val transaction = fragmentManager.beginTransaction()
                    val stackFragmentName = fragment.javaClass.name
                    var fragmentHasInStack = false
                    if (popAllStack) {
                        popWholeBackStack(fragmentManager)
                    } else {
                        fragmentHasInStack = popBackStackImmediate(stackFragmentName, 0)
                    }
                    if (!fragmentHasInStack) {
                        if (!handleTransactionReturnResultAvailability(fragment, transaction, false)) {
                            return
                        }
                        transaction.add(container, fragment, getFragmentTag(fragment))
                        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        transaction.addToBackStack(stackFragmentName)
                        transaction.commit()
                    } else {
                        throw Exception("something wrong")
                    }
                } catch (e: Exception) {
                    Log.e("FRAGMENT_ERROR", e.toString())
                }
            }
        }
    }

    fun openFragmentForce(fragment: Fragment?) {
        openFragmentForce(fragment, false)
    }

    fun openFragmentForceIgnoreThreshold(fragment: Fragment?) {
        openFragmentForce(fragment, true)
    }

    private fun openFragmentForce(fragment: Fragment?, ignoreThreshold: Boolean) {
        if (transitionAllowed()) {
            synchronized(lock) {
                try {
                    if (fragment == null) return
                    val stackFragmentName = fragment.javaClass.name + "_force"
                    val transaction = fragmentManager.beginTransaction()
                    if (!handleTransactionReturnResultAvailability(fragment, transaction, ignoreThreshold)) {
                        return
                    }
                    transaction.add(container, fragment, getFragmentTag(fragment))
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    transaction.addToBackStack(stackFragmentName)
                    transaction.commit()
                } catch (e: Exception) {
                    Log.e("FRAGMENT_ERROR", e.toString())
                }
            }
        }
    }

    private fun <T : Fragment?> getFragmentTag(clazz: Class<T>): String {
        return clazz.canonicalName
    }

    private fun getFragmentTag(fragment: Fragment): String {
        return getFragmentTag(fragment.javaClass)
    }

    fun resumeToFragment(name: String, openedForce: Boolean) {
        synchronized(lock) {
            try {
                val stackFragmentName = if (openedForce) name + "_force" else name
                fragmentManager.popBackStack(stackFragmentName, 0)
            } catch (e: Exception) {
                Log.e("FRAGMENT_ERROR", e.toString())
            }
        }
    }

    fun openFragmentForceWithoutHidingPrev(fragment: Fragment?) {
        if (transitionAllowed()) {
            synchronized(lock) {
                try {
                    if (fragment == null) return
                    val stackFragmentName = fragment.javaClass.name
                    val transaction = fragmentManager.beginTransaction()
                    transaction.add(container, fragment)
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    transaction.addToBackStack(stackFragmentName)
                    transaction.commit()
                } catch (e: Exception) {
                    Log.e("FRAGMENT_ERROR", e.toString())
                }
            }
        }
    }

    fun popBackStack() {
        if (transitionAllowed()) {
            synchronized(lock) {
                try {
                    fragmentManager.popBackStack()
                } catch (e: Exception) {
                    Log.e("FRAGMENT_ERROR", e.toString())
                }
            }
        }
    }

    fun popBackStackByTargetFragment(fragment: Fragment?) {
        if (transitionAllowed()) {
            synchronized(lock) {
                try {
                    if (fragment == null) return
                    fragmentManager.popBackStack(fragment.javaClass.name, 0)
                } catch (e: Exception) {
                    Log.e("FRAGMENT_ERROR", e.toString())
                }
            }
        }
    }

    val isCurrentFragmentLinked: Boolean
        get() {
            synchronized(lock) {
                return try {
                    val current = fragmentManager.fragments[fragmentManager.backStackEntryCount - 1]
                    current === linkFragment
                } catch (e: IndexOutOfBoundsException) {
                    true
                }
            }
        }

    fun replaceLinkFragmentAndSave(fragment: Fragment?) {
        linkFragment = fragment
        replaceFragment(fragment)
    }

    private fun replaceFragment(fragment: Fragment?) {
        if (transitionAllowed()) {
            synchronized(lock) {
                try {
                    if (fragment == null) return
                    val stackFragmentName = fragment.javaClass.name
                    val transaction = fragmentManager.beginTransaction()
                    if (!handleTransactionReturnResultAvailability(fragment, transaction, false)) {
                        return
                    }
                    transaction.replace(container, fragment)
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    transaction.addToBackStack(stackFragmentName)
                    transaction.commit()
                } catch (e: Exception) {
                    Log.e("FRAGMENT_ERROR", e.toString())
                }
            }
        }
    }

    fun openDialogFragment(dialogFragment: DialogFragment) {
        if (transitionAllowed()) {
            synchronized(lock) {
                try {
                    dialogFragment.show(fragmentManager, dialogFragment.javaClass.name)
                } catch (e: Exception) {
                    Log.e("FRAGMENT_ERROR", e.toString())
                }
            }
        }
    }

    fun stackIsNull(): Boolean {
        synchronized(lock) { return fragmentManager.backStackEntryCount <= 1 }
    }

    fun currentFragmentHaveNotEndedActions(): Boolean {
        synchronized(lock) { return currentFragmentCallback!!.hasNotEndedActions() }
    }

    fun notifyBackPressed() {
        if (transitionAllowed()) {
            synchronized(lock) {
                try {
                    currentFragmentCallback?.notifyBackPressed()
                } catch (e: Exception) {
                    Log.e("FRAGMENT_ERROR", e.toString())
                }
            }
        }
    }

    private fun transitionAllowed(): Boolean {
        return listener != null/* && listener.requestTransitionState() === TransactionState.ENABLED*/
    }

    private fun handleTransactionReturnResultAvailability(fragment: Fragment, transaction: FragmentTransaction, ignoreThreshold: Boolean): Boolean {
        return if (fragmentManager.findFragmentById(container) != null) {
            val currentFragment = fragmentManager.findFragmentById(container)
            val currentTime = System.currentTimeMillis()
            val transactionByTimeEnable = currentTime - lastTransaction.time < MIN_TRANSACTION_THRESHOLD
            val lastTransactionCurrent = currentFragment != null && lastTransaction.clazz == currentFragment.javaClass
            if (!ignoreThreshold && lastTransactionCurrent && transactionByTimeEnable) {
                return false
            }
            if (currentFragment != null) {
                transaction.hide(currentFragment)
            }
            saveLastTransaction(fragment)
            true
        } else {
            saveLastTransaction(fragment)
            true
        }
    }

    private fun saveLastTransaction(fragment: Fragment?) {
        if (fragment == null) return
        lastTransaction.clazz = fragment.javaClass
        lastTransaction.time = System.currentTimeMillis()
    }

    private inner class Transaction {
        var time: Long = 0
        var clazz: Class<*>? = null

    }

    companion object {
        private const val FRAGMENT_STACK_LIMIT = 6
        private const val MIN_TRANSACTION_THRESHOLD = 500
        private const val FRAGMENT_OPEN_THRESHOLD = 250
        private fun popBackStackImmediate(fragmentManager: FragmentManager, className: String, flags: Int): Boolean {
            return fragmentManager.popBackStackImmediate(className, flags)
        }

        private fun popBackStackImmediate(fragmentManager: FragmentManager, id: Int, flags: Int): Boolean {
            return fragmentManager.popBackStackImmediate(id, flags)
        }

        private fun popWholeBackStack(fragmentManager: FragmentManager) {
            if (fragmentManager.backStackEntryCount > 0) {
                val backStackEntry = fragmentManager.getBackStackEntryAt(0)
                if (backStackEntry != null) {
                    popBackStackImmediate(fragmentManager, backStackEntry.id, 0)
                }
            }
        }
    }

    init {
        fragmentManager.addOnBackStackChangedListener { synchronized(lock) { listener?.onNewScreenOpened(currentFragmentCallback) } }
    }
}