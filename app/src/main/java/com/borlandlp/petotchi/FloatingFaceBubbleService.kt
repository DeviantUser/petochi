package com.borlandlp.petotchi

import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.util.DisplayMetrics
import android.view.*
import androidx.annotation.RequiresApi
import androidx.core.app.JobIntentService
import com.borlandlp.petotchi.config.EntityConfig
import com.borlandlp.petotchi.drawer.EntityDrawer
import com.borlandlp.petotchi.drawer.SystemEventListener
import com.borlandlp.petotchi.entity.*
import com.borlandlp.petotchi.entity.animation.AnimationService
import com.borlandlp.petotchi.entity.behavior.BehaviorCreator
import com.borlandlp.petotchi.world.CollisionChecker
import com.borlandlp.petotchi.world.Location
import com.borlandlp.petotchi.world.World
import java.util.*

const val SERVICE_PAUSE = "com.petochi.PAUSE"
const val SERVICE_ADD_ENTITY = "com.petochi.SERVICE_ADD_ENTITY"

class FloatingFaceBubbleService : JobIntentService() {
    private var windowManager: WindowManager? = null

    private var entityWorld: World? = null
    private var entityDrawer: EntityDrawer? = null
    private var animationService: AnimationService? = null

    private var mInterval = (1000 / 60).toLong() // 60fps
    private var mHandler: Handler? = null
    private var mStatusChecker: Runnable? = null
    private var isDestroyed = false
    private val screenStatusReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.action?.let {
                if ("android.intent.action.SCREEN_OFF" == it) {

                } else if ("android.intent.action.SCREEN_ON" == it) {

                }
            }
        }
    }
    private var paused = false


    override fun onCreate() {
        super.onCreate()
        setForegroundNotification(true)
        windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val height = windowManager!!.maximumWindowMetrics.bounds.bottom
        val width = windowManager!!.maximumWindowMetrics.bounds.right
        animationService = AnimationService(this)

        entityWorld = World(
            entityController = EntityController(),
            leftEdge = 0,
            topEdge = 0,
            rightEdge = width,
            bottomEdge = height,
            collisionChecker = CollisionChecker()
        )
        entityDrawer = EntityDrawer(
            world = entityWorld!!,
            context = this,
            listener = SystemEventListener(),
            animationService = animationService!!
        )
        entityWorld!!.entityController.addEntity(LivingEntity(entityWorld!!).apply {
            this.width = (85 * EntityConfig.petSizeMultiplier).toInt()
            this.height = (85 * EntityConfig.petSizeMultiplier).toInt()
            location = Location(width / 2F, height / 2F)
            weight = 50 + Random().nextInt(50)
            temperament = Temperament.createRandom()
            behaviorDirector = BehaviorCreator.create(this)
        })

        generateBorder(entityWorld!!)

        mStatusChecker = Runnable {
            if (isDestroyed) {
                return@Runnable
            }

            try {
                if (!paused) {
                    entityWorld!!.update()
                    entityDrawer!!.update()
                }
            } finally {
                mHandler!!.postDelayed(mStatusChecker!!, mInterval)
            }
        }

        mHandler = Handler(Looper.getMainLooper())
        startRepeatingTask()
    }

    override fun onStartCommand(intent: Intent?, i: Int, i2: Int): Int {
        super.onStartCommand(intent, i, i2)
        intent?.let {
            when (it.action) {
                SERVICE_PAUSE -> paused = !paused
                SERVICE_ADD_ENTITY -> {
                    entityWorld!!.entityController.addEntity(LivingEntity(entityWorld!!).apply {
                        width = 100
                        height = 100
                        location = Location((windowManager!!.maximumWindowMetrics.bounds.right + kotlin.random.Random.nextInt(100)) / 2F, windowManager!!.maximumWindowMetrics.bounds.bottom / 2F)
                        weight = 50 + Random().nextInt(50)
                        temperament = Temperament.createRandom()
                        behaviorDirector = BehaviorCreator.create(this)
                    })
                }
            }
        }

        return START_STICKY
    }

    private fun generateBorder(world: World) {
        val borderWidth = 50
        // left
        world.entityController.addEntity(BorderEntity(world).apply {
            location = Location(y = borderWidth * 3F)
            width = borderWidth
            height = world.bottomEdge - (borderWidth * 3)
        })

        // top
        world.entityController.addEntity(BorderEntity(world).apply {
            location = Location()
            width = world.rightEdge
            height = borderWidth
        })

        // right
        world.entityController.addEntity(BorderEntity(world).apply {
            location = Location(x = (world.rightEdge - borderWidth).toFloat(), y = borderWidth * 3F)
            width = borderWidth
            height = world.bottomEdge - (borderWidth * 3)
        })

        // bottom
        world.entityController.addEntity(BorderEntity(world).apply {
            location = Location(y = (world.bottomEdge - borderWidth).toFloat())
            width = world.rightEdge
            height = borderWidth
        })
    }

    /* access modifiers changed from: private */
    private fun setForegroundNotification(petsVisible: Boolean = true) {
        val service = PendingIntent.getService(
            this,
            0,
            Intent(this, FloatingFaceBubbleService::class.java).setAction("TOGGLE_VISIBILITY"),
            0
        )
        val builder = Notification.Builder(this)
        val contentTitleStr = if (petsVisible) {
            "content title on"
        } else {
            "content title off"
        }
        val contentTitle = builder.setContentTitle(contentTitleStr)
        val contentTitleStr2 = if (petsVisible) {
            "content 2 title on"
        } else {
            "content 2 title off"
        }
        val contentIntent = contentTitle
            .setContentText(contentTitleStr2)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
            .setOngoing(true)
            .setPriority(Notification.PRIORITY_MIN)
            .setContentIntent(service)
        if (Build.VERSION.SDK_INT >= 26) {
            setupNotificationChannel()
            contentIntent.setChannelId("notification_channel")
        }
        val build = contentIntent.build()
        if (petsVisible) {
            startForeground(99, build)
            return
        }
        (getSystemService(NOTIFICATION_SERVICE) as NotificationManager?)?.notify(99, build)
    }

    @RequiresApi(api = 26)
    private fun setupNotificationChannel() {
        val string = "Toggles"
        val string2 = "Allows user to quickly hide/show petochi from notification"
        val notificationChannel =
            NotificationChannel("notification_channel", string, NotificationManager.IMPORTANCE_MIN)
        notificationChannel.description = string2
        (getSystemService(NOTIFICATION_SERVICE) as NotificationManager?)?.createNotificationChannel(
            notificationChannel
        )
    }

    private fun startRepeatingTask() {
        mStatusChecker!!.run()
    }
    private fun stopRepeatingTask() {
        mHandler!!.removeCallbacks(mStatusChecker!!)
    }

    override fun onHandleWork(intent: Intent) {

    }

    override fun onDestroy() {
        super.onDestroy()
        isDestroyed = true
        stopRepeatingTask()
        entityDrawer?.onDestroy()
        entityWorld?.onDestroy()
        stopForeground(true)
        windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
    }
}