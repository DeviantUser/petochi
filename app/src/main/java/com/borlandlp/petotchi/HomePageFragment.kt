package com.borlandlp.petotchi

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment.findNavController

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomePageFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomePageFragment : Fragment(), NamedFragmentCallback {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_page, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<View>(R.id.add_entity).setOnClickListener {
//            findNavController(this).navigate(R.id.settingsFragment)
            val serviceIntent = Intent(context, FloatingFaceBubbleService::class.java)
            serviceIntent.action = SERVICE_ADD_ENTITY
            serviceIntent.putExtra("UserID", "123456")
            requireContext().startService(serviceIntent)
        }
        view.findViewById<View>(R.id.start_anim_btn).setOnClickListener {
            (context as MainActivity).startPetService()
        }
        view.findViewById<View>(R.id.stop_anim_btn).setOnClickListener {
            (context as MainActivity).stopPetService()
        }
        view.findViewById<View>(R.id.pause_anim_btn).setOnClickListener {
            val serviceIntent = Intent(context, FloatingFaceBubbleService::class.java)
            serviceIntent.action = SERVICE_PAUSE
            serviceIntent.putExtra("UserID", "123456")
            requireContext().startService(serviceIntent)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        (context as MainActivity).stopPetService()
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            HomePageFragment().apply {

            }
    }

    override fun getFragmentName(context: Context?) = "test"
    override fun getAnalyticFragmentName(context: Context?) = "test"
}