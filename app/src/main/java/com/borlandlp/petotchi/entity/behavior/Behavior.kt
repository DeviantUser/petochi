package com.borlandlp.petotchi.entity.behavior

import com.borlandlp.petotchi.entity.Entity
import com.borlandlp.petotchi.entity.EntityEvent
import com.borlandlp.petotchi.entity.TemperamentType
import com.borlandlp.petotchi.entity.WorldEvent

abstract class Behavior(var entity: Entity?) {
    abstract val type: TemperamentType
    var listener: Listener? = null
    open var isBackground = false
    open var isActive = false

    /**
     * Вызывается множество раз в секунду, при тике логики сущностей
     */
    abstract fun update()

    /**
     * Вызывается для опроса поведения, хочет ли оно мягко перехватить контроль
     */
    open fun isWantTakeControl(): Boolean {
        return false
    }

    /**
     * Вызывается для опроса поведения, хочет ли оно принудительно перехватить контроль
     */
    open fun isWantForceTakeControl(): Boolean {
        return false
    }

    /**
     * Вызывается, когда поведению передается управление
     */
    abstract fun onStart()

    /**
     * Вызывается, когда поведение теряет контроль
     *
     * @param force Завершается ли принудительно
     */
    abstract fun onStop(force: Boolean)

    open fun onCreate(iListener: Listener) {
        listener = iListener
    }

    open fun onEntityEvent(event: EntityEvent) {

    }

    open fun onWorldEvent(event: WorldEvent) {

    }
}