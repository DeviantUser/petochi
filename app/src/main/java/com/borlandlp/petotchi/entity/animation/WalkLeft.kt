package com.borlandlp.petotchi.entity.animation

import android.graphics.Bitmap

open class WalkLeft(bitmap: Bitmap, height: Int, width: Int) : Animation(bitmap, height, width) {
    override val id: Int = 1
    override val rowPosition = 3
}