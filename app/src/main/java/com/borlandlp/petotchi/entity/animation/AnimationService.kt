package com.borlandlp.petotchi.entity.animation

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import com.borlandlp.petotchi.R
import com.borlandlp.petotchi.config.EntityConfig
import com.borlandlp.petotchi.utils.BitmapHelper
import com.borlandlp.petotchi.entity.Entity

class AnimationService(val context: Context) {
    private val mapping = mapOf(
        AnimationId.WALK_RIGHT.id to R.drawable.bear_sprite,
        AnimationId.SIT_RIGHT.id to R.drawable.bear_sprite
    )
    private val bitmapCache = mutableMapOf<Int, Drawable>()

    fun get(animData: AnimationId, entity: Entity): Animation {
        val drawable = if (bitmapCache.containsKey(animData.id)) {
            bitmapCache[animData.id]!!
        } else {
            var newDrawable = if (animData.parentId != 0) {
                val source = ContextCompat.getDrawable(context, mapping[animData.parentId]!!)
                BitmapHelper.flip(source!! as BitmapDrawable)
            } else {
                ContextCompat.getDrawable(context, mapping[animData.id] ?: error("drawable not exists"))
            }
            val bitmap = BitmapHelper.getResizedBitmap(BitmapHelper.drawableToBitmap(newDrawable!!), EntityConfig.petSizeMultiplier)
            newDrawable = BitmapHelper.bitmapToDrawable(bitmap, context)

            bitmapCache[animData.id] = newDrawable
            newDrawable
        }

        return when (animData.id) {
            AnimationId.WALK_RIGHT.id -> WalkRight(bitmap = BitmapHelper.drawableToBitmap(drawable), height = entity.height, width = entity.width)
            AnimationId.WALK_LEFT.id -> WalkLeft(bitmap = BitmapHelper.drawableToBitmap(drawable), height = entity.height, width = entity.width)
            AnimationId.SIT_LEFT.id -> SitLeft(bitmap = BitmapHelper.drawableToBitmap(drawable), height = entity.height, width = entity.width)
            AnimationId.SIT_RIGHT.id -> SitRight(bitmap = BitmapHelper.drawableToBitmap(drawable), height = entity.height, width = entity.width)
            else -> StandingAnimation(bitmap = BitmapHelper.drawableToBitmap(drawable), height = entity.height, width = entity.width)
        }
    }

    private data class AnimationData(val animation: Animation, val pathId: Int)

    private enum class AnimationDrawable(val path: Int) {

    }
}