package com.borlandlp.petotchi.entity.behavior

import com.borlandlp.petotchi.entity.Entity

class BehaviorCreator {
    companion object {
        fun create(forEntity: Entity): BehaviorDirector {
            return BehaviorDirector
                .Builder()
                .entity(forEntity)
                .behaviorList(
                    listOf(
                        WalkingBehavior(forEntity),
                        SleepingBehavior(forEntity),
                        SittingBehavior(forEntity),
                        ClimbingBehavior(forEntity),
                        ClimbAnObstacleBehavior(forEntity)
                    )
                )
                .build()
        }
    }
}