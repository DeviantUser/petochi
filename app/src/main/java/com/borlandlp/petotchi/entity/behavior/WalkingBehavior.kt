package com.borlandlp.petotchi.entity.behavior

import android.util.Log
import com.borlandlp.petotchi.entity.*
import com.borlandlp.petotchi.world.DirectionList
import com.borlandlp.petotchi.world.Location
import com.borlandlp.petotchi.world.RawDirection
import java.lang.Exception
import java.util.*

class WalkingBehavior(entity: Entity) : Behavior(entity) {
    override val type = TemperamentType.ACTIVENESS

    override fun onWorldEvent(event: WorldEvent) {
        super.onWorldEvent(event)
        if (event is MovePointReachedEvent) {
            listener?.onWantsReleaseControl(this)
        }
    }

    override fun update() {
        if (entity !is LivingEntity) {
            throw Exception()
        }
        val castedEntity = entity as LivingEntity
        if (castedEntity.movingPoint == null && castedEntity.isOnGround()) {
            val leftCollision = entity!!.world.entityHaveCollisionForDirection(entity!!, DirectionList.LEFT)
            val rightCollision = entity!!.world.entityHaveCollisionForDirection(entity!!, DirectionList.RIGHT)
            // Если нельзя двигаться в обе стороны - завершаем ходьбу
            if (leftCollision != null && rightCollision != null) {
                Log.d("xxxxxaaaa", "walk: failed to find move point - left&&right collision")
                listener?.onWantsReleaseControl(this)
                return
            }

            // Пытаемся рандомно выбрать сторону следования из доступных
            var movingDirection = when {
                leftCollision == null && Random().nextBoolean() -> {
                    DirectionList.LEFT
                }
                rightCollision == null && Random().nextBoolean() -> {
                    DirectionList.RIGHT
                }
                leftCollision == null -> { // Если роллы предыдущих вариантов не прошли, пытаемся хоть сюда
                    DirectionList.LEFT
                }
                else -> { // крайний вариант
                    DirectionList.RIGHT
                }
            }

            val movingPoint = when (movingDirection) {
                DirectionList.LEFT -> {
                    entity!!.world.raycast(
                        entity = entity!!,
                        rawDirection = RawDirection(x = -1F, y = 0F),
                        range = -1F,
                        beamWidth = entity!!.height / 2
                    ).firstOrNull { it !is LivingEntity }?.let {
                        val maxX = it.getBoundingBox().right + 1
                        val distX = entity!!.location.x - maxX
                        var futureX = if (distX.toInt() > 0) {
                            Random().nextInt(distX.toInt()) + maxX
                        } else {
                            maxX
                        }
                        if (futureX - maxX < entity!!.width) {
                            futureX = maxX
                        }
                        Location(
                            y = entity!!.location.y,
                            x = futureX
                        )
                    }
                }
                DirectionList.RIGHT -> {
                    entity!!.world.raycast(
                        entity = entity!!,
                        rawDirection = RawDirection(x = 1F, y = 0F),
                        range = -1F,
                        beamWidth = entity!!.height / 2
                    ).firstOrNull { it !is LivingEntity }?.let {
                        val maxX = it.getBoundingBox().left - 1
                        val distX = maxX - entity!!.location.x
                        var futureX = maxX - Random().nextInt(distX.toInt())
                        if (maxX - futureX < entity!!.width) {
                            futureX = maxX
                        }
                        Location(
                            y = entity!!.location.y,
                            x = futureX
                        )
                    }
                }
                else -> throw Exception("")
            }
            (entity as LivingEntity).movingPoint = movingPoint
            Log.d("xxxxxaaaa", "start walk loc:${(entity as LivingEntity).movingPoint}")
        }
    }

    override fun onStart() {
    }

    override fun onStop(force: Boolean) {

    }
}