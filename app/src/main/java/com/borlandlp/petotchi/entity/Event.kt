package com.borlandlp.petotchi.entity

import com.borlandlp.petotchi.world.Location

abstract class Event {
    var isCanceled = false
}

abstract class WorldEvent : Event() {}

abstract class EntityEvent(val entity: Entity) : WorldEvent() {}

class MovePointReachedEvent(var point: Location) : WorldEvent()

class EntityCollisionWithEntityEvent(entity: Entity, val with: Entity) : EntityEvent(entity)

class EntityTapEvent(entity: Entity, val tapPoint: Location) : EntityEvent(entity)

class EntityMoveByTapEvent(entity: Entity, val newPoint: Location) : EntityEvent(entity)