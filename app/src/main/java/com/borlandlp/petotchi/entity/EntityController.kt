package com.borlandlp.petotchi.entity

/**
 * обновление состояния энтити при тике
 */
class EntityController {
    private val entities: MutableList<Entity> = mutableListOf()

    fun addEntity(entity: Entity) {
        entities.add(entity)
    }

    fun removeEntity(entity: Entity) {
        entities.remove(entity)
    }

    fun getEntities(): List<Entity> {
        return entities
    }

    fun update() {
        entities.forEach { it.tick() }
    }

    fun onDestroy() {
        entities.removeAll { true }
    }
}