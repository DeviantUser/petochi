package com.borlandlp.petotchi.entity.behavior

import android.util.Log
import com.borlandlp.petotchi.entity.*
import com.borlandlp.petotchi.world.DirectionList
import com.borlandlp.petotchi.world.Location
import com.borlandlp.petotchi.world.RawDirection

class ClimbAnObstacleBehavior(entity: Entity) : Behavior(entity) {
    override val type = TemperamentType.ACTIVENESS
    override var isBackground = true
    private var isWantTakeControl = false
    private var isWantTakeForceControl = false

    override fun isWantTakeControl(): Boolean {
        return isWantTakeControl
    }

    override fun isWantForceTakeControl(): Boolean {
        return isWantTakeForceControl
    }

    override fun update() {

    }

    override fun onStart() {
        Log.d("xxxxxaaaa", "start climb an obstacle")
        isWantTakeControl = false

        val horizontalCollision = entity!!.world.entityHaveCollisionForDirection(entity!!, entity!!.faceDirection)
        (entity as LivingEntity).movingPoint = if (entity!!.faceDirection == DirectionList.LEFT) {
            Location(
                x = horizontalCollision!!.getBoundingBox().right + (entity!!.width / 2.5F),
                y = horizontalCollision.getCenterLocation().y
            )
        } else {
            Location(
                x = horizontalCollision!!.getBoundingBox().left - (entity!!.width / 2.5F),
                y = horizontalCollision.getCenterLocation().y
            )
        }
        isActive = true
    }

    override fun onStop(force: Boolean) {
        isActive = false
    }

    override fun onWorldEvent(event: WorldEvent) {
        super.onWorldEvent(event)
        if (event is EntityCollisionWithEntityEvent && !isActive && event.with is LivingEntity) {
            if (event.with.height > entity!!.height) {
                isWantTakeForceControl = true
            }
        } else if (event is MovePointReachedEvent) {
            // TODO
        }
    }
}