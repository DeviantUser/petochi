package com.borlandlp.petotchi.entity.animation

import android.graphics.Bitmap

open class SitRight (bitmap: Bitmap, height: Int, width: Int) : Animation(bitmap, height, width) {
    override val id: Int = 4
    override val rowPosition = 5
}