package com.borlandlp.petotchi.entity.animation

/** parentId указывает на то, что спрайты берутся из родительского ID и просто переворачиваются по оси Х на 180 */
enum class AnimationId(val id: Int, val parentId: Int) {
    WALK_RIGHT(id = 1, parentId = 0),
    WALK_LEFT(id = 2, parentId = 1),
    SIT_RIGHT(id = 3, parentId = 0),
    SIT_LEFT(id = 4, parentId = 3),
}