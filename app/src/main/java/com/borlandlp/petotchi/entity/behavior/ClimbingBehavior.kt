package com.borlandlp.petotchi.entity.behavior

import android.util.Log
import com.borlandlp.petotchi.entity.*
import com.borlandlp.petotchi.world.DirectionList
import com.borlandlp.petotchi.world.Location
import com.borlandlp.petotchi.world.RawDirection

class ClimbingBehavior(entity: Entity?) : Behavior(entity) {
    override val type = TemperamentType.ACTIVENESS
    override var isBackground = true
    private var isWantTakeControl = false

    override fun isWantTakeControl(): Boolean {
        return isWantTakeControl
    }
    override fun update() {

    }

    override fun onStart() {
        Log.d("xxxxxaaaa", "start climb")
        isWantTakeControl = false

        val horizontalCollision = entity!!.world.entityHaveCollisionForDirection(entity!!, entity!!.faceDirection)
        (entity as LivingEntity).movingPoint = if (entity!!.faceDirection == DirectionList.LEFT) {
            Location(
                x = horizontalCollision!!.getBoundingBox().right + (entity!!.width / 2.5F),
                y = horizontalCollision.getCenterLocation().y
            )
        } else {
            Location(
                x = horizontalCollision!!.getBoundingBox().left - (entity!!.width / 2.5F),
                y = horizontalCollision.getCenterLocation().y
            )
        }
        isActive = true
    }

    override fun onStop(force: Boolean) {
        isActive = false
    }

    override fun onWorldEvent(event: WorldEvent) {
        super.onWorldEvent(event)
        if (!isActive && event is MovePointReachedEvent) {
            val horizontalCollision = entity!!.world.entityHaveCollisionForDirection(entity!!, entity!!.faceDirection)
            if (horizontalCollision != null) {
                isWantTakeControl = true
            }
        } else if (isActive && event is MovePointReachedEvent) {
            (entity as LivingEntity).jump(RawDirection(x = if (entity!!.faceDirection == DirectionList.RIGHT) -1F else 1F, y = 1F))
            listener?.onWantsReleaseControl(this)
        }
    }
}