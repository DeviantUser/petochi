package com.borlandlp.petotchi.entity

import com.borlandlp.petotchi.CollisionEvent
import com.borlandlp.petotchi.CollisionEventHub
import com.borlandlp.petotchi.CollisionType
import com.borlandlp.petotchi.entity.behavior.BehaviorDirector
import com.borlandlp.petotchi.world.*
import kotlin.math.min
import kotlin.random.Random

abstract class Entity(var world: World) {
    open var gravityImmunity: Boolean = false
    open var location = Location()
    open val canCollideWithOtherEntity = false
    open var behaviorDirector: BehaviorDirector? = null
    open var isTouched = false
    open var isTouchable = true
    open var height = 100
    open var width = 100
    open var weight = 50
    open var acceleration = 0F
    /** Прыгучесть. Эта единица будет составлять в процентах остаточную противоположную скорость после столкновения */
    open var hardness = 50F
    open var velocity = Velocity(0F, 0F)
    open val isFalling: Boolean
        get() = velocity.y > 0F
    open var temperament = Temperament()
    open var faceDirection = DirectionList.LEFT

    open fun tick() {
        if (!isTouched) {
            updateGravity()
            if (velocity.x > 0) {
                faceDirection = DirectionList.RIGHT
            } else if (velocity.x < 0) {
                faceDirection = DirectionList.LEFT
            }
        }
        behaviorDirector?.tick()
    }

    open fun getCenterLocation(): Location {
        return Location(
            x = location.x + (width / 2),
            y = location.y + (height / 2)
        )
    }

    open fun getBoundingBox() = BoundingBox(
        left = location.x,
        right = location.x + width,
        bottom = location.y + height,
        top = location.y
    )

    open fun isOnGround(): Boolean {
        val nearbyEntities = world.raycast(entity = this, rawDirection = RawDirection(0F, 1F), beamWidth = width) // TODO Поправить дальность луча
        val collideDetect = nearbyEntities.filter {  worldEntity ->
            world.checkCollision(from = this, to = worldEntity, fromEntityOffset = Offset(0F, 0F, 0F, 50F))
        }
        val result = collideDetect.filter { world.getYDirectionFromEntityToEntity(this, it) == DirectionList.DOWN }
        result.forEach {
            CollisionEventHub.add(CollisionEvent(
                from = this,
                to = it,
                type = CollisionType.DOWN
            ))
        }
        return result.isNotEmpty()
    }

    protected open fun updateGravity() {
        if (isOnGround() && isFalling) { // отскок при падении по Y
            val currentVelocity = velocity
            if (currentVelocity.y > 3) {
                val bounceCoefficient = -((100 - hardness) / 100)
                currentVelocity.y = currentVelocity.y * bounceCoefficient
            } else {
                currentVelocity.y = 0.0F
            }
            velocity = currentVelocity
        }

        if(!isOnGround() && !gravityImmunity) { // gravity
            val currentVelocity = velocity
            if (currentVelocity.y < world.maximumSpeedOfFreeFall) {
                currentVelocity.y += world.gravity
            }
        } else if (isOnGround() && velocity.y > 0) {
            velocity.y = 0F
        }

        // Обработка Y velocity
        val vDirection = DirectionList.rawDirectionToYDirection(RawDirection(x = velocity.x, y = velocity.y))
        val verticalCollision = world.entityHaveCollisionForDirection(this, vDirection)
        if (verticalCollision == null && velocity.y != 0.0F) {
            location = Location(x = location.x, y = location.y + velocity.y)
        }

        // Обработка X коллизий
        val xDirection = DirectionList.rawDirectionToXDirection(RawDirection(x = velocity.x, y = velocity.y))
        val horizontalCollision = world.entityHaveCollisionForDirection(this, xDirection)?.apply {
            if (CollisionEventHub.enabled) {
                CollisionEventHub.add(
                    CollisionEvent(
                        from = this,
                        to = this@Entity,
                        type = if (xDirection == DirectionList.LEFT) CollisionType.LEFT else CollisionType.RIGHT
                    )
                )
            }
        }
        if (horizontalCollision != null) {
            // horizontalCollision - будущее столкновение, с учетом велосити. проверим, можем ли мы сдвинуться на велосити, меньше текущего
            val offset = world.getOffsetBetweenTwoEntities(this, horizontalCollision)
            if (faceDirection == DirectionList.RIGHT && offset.right > 1) {
                velocity.x = offset.right
            } else if (faceDirection == DirectionList.LEFT && offset.left < -1) {
                velocity.x = offset.left
            } else {
                velocity.x = 0F
                onXCollisionDetect(with = horizontalCollision)
            }
        }

        // гашение инерции движения по X
        if (velocity.x != 0F) {
            val vX = if ((velocity.x > 0 && velocity.x <= 0.1) || (velocity.x < 0 && velocity.x >= -0.1)) {
                velocity.x
            } else {
                if (velocity.x > 0F) 0.1F else -0.1F
            }

            velocity.reduce(Velocity(x = vX, y = 0F))
        }

//        if (this.#positionY > window.screen.availHeight + 100) {
//            this.getWorld().removeEntity(this);
//            console.log('Entity reached end of world. Removing...');
//        }
    }

    protected open fun isCollideWith(entity: Entity): Boolean {
        return false
    }

    protected open fun onXCollisionDetect(with: Entity) {}

    protected open fun destroy() {
        behaviorDirector = null
    }

    data class BoundingBox(
        val left: Float,
        val right: Float,
        val bottom: Float,
        val top: Float
    )
}

open class LivingEntity(world: World) : Entity(world) {
    var hp: Float = 100F
    var satiety: Float = 0F
    var movingPoint: Location? = null
        set(value) {
            field = value
            velocity = Velocity(0F, 0F)
            if (movingPoint != null) {
                faceDirection = if (movingPoint!!.x <= getCenterLocation().x) {
                    DirectionList.LEFT
                } else {
                    DirectionList.RIGHT
                }
            }
        }
    var movingHDirection: DirectionList = DirectionList.NONE
    val movingVDirection: DirectionList
        get() = when {
            velocity.y > 0 -> {
                DirectionList.DOWN
            }
            velocity.y < 0 -> {
                DirectionList.UP
            }
            else -> {
                DirectionList.NONE
            }
        }
    open var isSitting = false
    open var isSneaking = false
    open var isClimbing = false
    var isAlive = true
    val isJumping: Boolean
        get() = !isClimbing && velocity.y < 0F
    var jumpPower = 15F
    var speed = 10F
    override var hardness: Float = 90F

    override fun tick() {
        super.tick()
        if (movingPoint != null) {
            if (isReachedMovingPoint()) {
                movingPoint = null
                behaviorDirector?.onWorldEvent(MovePointReachedEvent(location))
                return
            }

            val horizontalCollision = world.entityHaveCollisionForDirection(this, faceDirection)
            val vCollision = world.entityHaveCollisionForDirection(this, movingVDirection)
            if (horizontalCollision == null) {
                if (isClimbing && gravityImmunity) {
                    isClimbing = false
                    gravityImmunity = false
                }

                proceedWalkToPoint()
            } else {
                if (vCollision == null) {
                    isClimbing = true
                    gravityImmunity = true
                    proceedClimbToPoint()
                } else {
                    // TODO А что делать, если уперлись в вышестоящее препятствие, при движении вверх?
                }
            }
        }

        // Обработка X velocity
        if (velocity.x != 0.0F && !isTouched) {
            location = Location(x = location.x + velocity.x, y = location.y)
        }
    }

    protected fun proceedClimbToPoint() {
        // climb
        if (movingPoint!!.y < location.y) {
            velocity.x = 0F
            velocity.y = -speed
        }
    }

    protected open fun proceedWalkToPoint() {
        if (location.isEqual(movingPoint!!)) {
            return
        } else if (isJumping || isFalling || !isOnGround()) {
            return
        }

        movingHDirection = if (movingPoint!!.x < location.x) {
            DirectionList.LEFT
        } else {
            DirectionList.RIGHT
        }

        // jump
        val entitiesOnWay = world.raycast(entity = this, rawDirection = DirectionList.getRawDirection(movingHDirection, DirectionList.NONE), range = width.toFloat() * 2.5F, beamWidth = height - 1)
        val nearestEntity = if (entitiesOnWay.isNotEmpty()) entitiesOnWay[0] else null
        if (nearestEntity != null) {
            val isEntityMoveOnMe = !velocitiesHasSameDirections(nearestEntity.velocity, velocity)
            val isOvertakingEntity = !isEntityMoveOnMe && nearestEntity.velocity.x < velocity.x
            val isMotionlessEntity = nearestEntity.velocity.x == 0F

            if (isEntityMoveOnMe && nearestEntity.weight > weight) {
                jump(DirectionList.getRawDirection(movingHDirection, DirectionList.UP). apply {
                    y = -1F
                    x = if (movingHDirection == DirectionList.LEFT) -0.1F else 0.1F
                })
                return
            }
        }

        // walk
        if (!isTouched) {
            velocity.x = if (movingHDirection == DirectionList.LEFT) -speed else speed
        }
    }

    private fun isReachedMovingPoint(): Boolean {
        return world.isCollide(getBoundingBox(), BoundingBox(
            left = movingPoint!!.x-1,
            right = movingPoint!!.x+1,
            bottom = movingPoint!!.y+1,
            top = movingPoint!!.y-1
        ))
    }

    override fun onXCollisionDetect(with: Entity) {
        super.onXCollisionDetect(with)
        if (!isClimbing) {
            behaviorDirector?.onEntityEvent(EntityCollisionWithEntityEvent(entity = this, with = with))
        }
    }

    private fun velocitiesHasSameDirections(v1: Velocity, v2: Velocity): Boolean {
        return v1.x > 0 && v2.x > 0 || v1.x < 0 && v2.x < 0
    }

    fun jump(rawDirection: RawDirection = RawDirection(0F, 1F)) {
        val xVelocity = if (rawDirection.x == 0F) {
            0F
        } else {
            jumpPower * min(rawDirection.x, if (rawDirection.x > 0) 1F else -1F)
        }
        val yVelocity = if (rawDirection.y == 0F) {
            0F
        } else {
            jumpPower * min(rawDirection.y, if (rawDirection.y > 0) 1F else -1F)
        }

        if (rawDirection.x > 0) {
            faceDirection = DirectionList.RIGHT
        } else if (rawDirection.x < 0) {
            faceDirection = DirectionList.LEFT
        }

        if (isClimbing && gravityImmunity) {
            isClimbing = false
            gravityImmunity = false
        }

        velocity = Velocity(
            x = xVelocity,
            y = yVelocity
        )
    }
}

class Temperament {
    private val data = mutableMapOf<TemperamentType, Float>()

    fun set(type: TemperamentType, value: Float) {
        data[type] = value
    }

    fun get(type: TemperamentType): Float {
        return if (data.containsKey(type)) data[type]!! else 0F
    }

    companion object {
        fun createRandom(): Temperament {
            return Temperament().apply {
                TemperamentType.values().forEach {
                    set(it, Random.nextFloat() * 90)
                }
            }
        }
    }
}

enum class TemperamentType(val id: String) {
    ACTIVENESS("ACTIVENESS"),
    CURIOSITY("CURIOSITY"), // Любопытство
    AGGRESSIVENESS("AGGRESSIVENESS"),
    PERSEVERANCE("PERSEVERANCE"), // (настойчивость, непоколебимость)
    INGENUITY("INGENUITY"), // (изобретательность, мастерство)
    COURAGE("COURAGE"), // (смелость, мужество, отвага)
    ENDURANCE("ENDURANCE"), // (выносливость, стойкость)
    GLUTTONY("GLUTTONY"); // прожорливость
}
