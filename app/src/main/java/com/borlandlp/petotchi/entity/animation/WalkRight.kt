package com.borlandlp.petotchi.entity.animation

import android.graphics.Bitmap

open class WalkRight (bitmap: Bitmap, height: Int, width: Int) : Animation(bitmap, height, width) {
    override val id: Int = 2
    override val rowPosition = 3
}