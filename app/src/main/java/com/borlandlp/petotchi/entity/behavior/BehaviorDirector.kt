package com.borlandlp.petotchi.entity.behavior

import com.borlandlp.petotchi.entity.Entity
import com.borlandlp.petotchi.entity.EntityEvent
import com.borlandlp.petotchi.entity.WorldEvent
import kotlin.random.Random

class BehaviorDirector private constructor(
    private var activeBehavior: Behavior?,
    private var behaviorList: List<Behavior>,
    private var entity: Entity
) {

    init {
        behaviorList.forEach {
            it.onCreate(object : Listener {
                override fun onWantsReleaseControl(behavior: Behavior) {
                    behavior.onStop(false)
                    activeBehavior = null
                }
            })
        }
    }

    /**
     * Обновляет состояние поведения.
     * Если активного состояния нет - выбирает новое активное:
     * 1) Сперва опрашивает список поведений, хотят ли они принудительно перехватить управление
     * 2) Сперва опрашивает список поведений, хотят ли они мягко перехватить управление
     * 3) Если нет, то роллит шансы активации для всех поведений, в зависимости от темперамента
     * 4) Если чудо не произошло, и никому ничего не выпало то выбираем случайный
     */
    fun tick() {
        if (activeBehavior == null) {
            appointNewBehavior()
        }
        activeBehavior?.update()
    }

    private fun appointNewBehavior() {
        val wantForceTakeControl: Behavior? = behaviorList.firstOrNull {
            it.isWantForceTakeControl()
        }
        if (wantForceTakeControl != null) {
            activeBehavior = wantForceTakeControl
            activeBehavior!!.onStart()
            return
        }

        val wantTakeControl: Behavior? = behaviorList.firstOrNull {
            it.isWantTakeControl()
        }
        if (wantTakeControl != null) {
            activeBehavior = wantTakeControl
            activeBehavior!!.onStart()
            return
        }

        val rollNewBehavior: Behavior? = behaviorList.firstOrNull {
            !it.isBackground && entity.temperament.get(it.type) > Random.nextFloat() * 100
        }
        if (rollNewBehavior != null) {
            activeBehavior = rollNewBehavior
            activeBehavior!!.onStart()
            return
        }

        val behaviors = behaviorList.filter { !it.isBackground }
        activeBehavior = behaviors[Random.nextInt(behaviors.size - 1)]
        activeBehavior?.onStart()
    }

    fun onEntityEvent(event: EntityEvent) {
        behaviorList.forEach { it.onEntityEvent(event) }
    }

    fun onWorldEvent(event: WorldEvent) {
        behaviorList.forEach { it.onWorldEvent(event) }
    }

    class Builder {
        private var activeBehavior: Behavior? = null
        private var behaviorList: List<Behavior> = listOf()
        private var entity: Entity? = null

        fun entity(entity: Entity) = apply { this.entity = entity }
        fun behaviorList(list: List<Behavior>) = apply { this.behaviorList = list }
        fun activeBehavior(behavior: Behavior) = apply { this.activeBehavior = behavior }

        fun build() = BehaviorDirector(activeBehavior, behaviorList, entity!!)
    }
}

interface Listener {
    fun onWantsReleaseControl(behavior: Behavior)
}