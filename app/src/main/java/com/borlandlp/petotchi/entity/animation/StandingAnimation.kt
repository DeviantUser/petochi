package com.borlandlp.petotchi.entity.animation

import android.graphics.Bitmap

open class StandingAnimation (bitmap: Bitmap, height: Int, width: Int) : Animation(bitmap, height, width) {
    override val id: Int = 6
    override val rowPosition = 4
}