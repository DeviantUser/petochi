package com.borlandlp.petotchi.entity.behavior

import com.borlandlp.petotchi.entity.Entity
import com.borlandlp.petotchi.entity.TemperamentType

class EmptyBehavior(entity: Entity) : Behavior(entity) {
    override val type = TemperamentType.ACTIVENESS

    override fun update() {

    }

    override fun onStart() {

    }

    override fun onStop(force: Boolean) {

    }
}