package com.borlandlp.petotchi.entity.animation

import android.graphics.Bitmap

open class SitLeft (bitmap: Bitmap, height: Int, width: Int) : Animation(bitmap, height, width) {
    override val id: Int = 5
    override val rowPosition = 5
}