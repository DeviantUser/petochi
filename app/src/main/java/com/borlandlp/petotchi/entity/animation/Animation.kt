package com.borlandlp.petotchi.entity.animation

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Rect

abstract class Animation(val bitmap: Bitmap, var height: Int, var width: Int) { // 50x50
    var currentFrame = 0
    open val rowPosition = 3
    abstract val id: Int

    /**
     * Вызывается при тике отрисовки, должен переключать кадры или какая-то промежуточная логика
     */
    fun update(canvas: Canvas) {
        val sprites = 18
        if (++currentFrame >= sprites) {
            currentFrame = 1
        }
        height = bitmap.height / 12
        width = bitmap.width / 18
        val srcX = currentFrame * width
        val srcY = rowPosition * height
        val src = Rect(srcX, srcY, srcX + width, srcY + height)
        val dst = Rect(0, 0, 0 + width, 0 + height)

        canvas.drawBitmap(bitmap, src, dst, null)
    }
}