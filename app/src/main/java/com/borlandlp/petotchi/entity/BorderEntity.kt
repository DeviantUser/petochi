package com.borlandlp.petotchi.entity

import com.borlandlp.petotchi.world.World

class BorderEntity(world: World) : Entity(world) {
    override var gravityImmunity = true
    override var isTouchable = false

    override fun tick() {

    }
}