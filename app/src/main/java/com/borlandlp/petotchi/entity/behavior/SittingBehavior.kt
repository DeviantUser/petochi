package com.borlandlp.petotchi.entity.behavior

import android.util.Log
import com.borlandlp.petotchi.entity.Entity
import com.borlandlp.petotchi.entity.LivingEntity
import com.borlandlp.petotchi.entity.TemperamentType
import kotlin.random.Random

class SittingBehavior(entity: Entity?) : Behavior(entity) {
    override val type = TemperamentType.ACTIVENESS

    private var waitTime = 0L
    private var waitStartTime = 0L

    override fun update() {
        if (waitStartTime + waitTime < System.currentTimeMillis()) {
            listener!!.onWantsReleaseControl(this)
        }
    }

    override fun onStart() {
        Log.d("xxxxxaaaa", "start sit")
        waitTime = Random.nextInt(1000, 10000).toLong()
        waitStartTime = System.currentTimeMillis()
        if (entity is LivingEntity) {
            (entity as LivingEntity).isSitting = true
        }
    }

    override fun onStop(force: Boolean) {
        if (entity is LivingEntity) {
            (entity as LivingEntity).isSitting = false
        }
    }
}