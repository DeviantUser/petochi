package com.borlandlp.petotchi

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat


class MainActivity : AppCompatActivity() {
    var fragmentLauncher: FragmentLauncher? = null
    var sensorManager: SensorManager? = null
    var sensorAccel: Sensor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        setContentView(R.layout.activity_main)
        fragmentLauncher = FragmentLauncher(this, null, object : FragmentLauncher.Listener {
            override fun onNewScreenOpened(fragmentCallback: NamedFragmentCallback?) {
                val a = 1
            }
        })
        sensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
        sensorAccel = sensorManager!!.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR)

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.SYSTEM_ALERT_WINDOW) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.SYSTEM_ALERT_WINDOW), 1)
        }
    }

    fun startPetService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(Intent(this, FloatingFaceBubbleService::class.java))
        } else {
            startService(Intent(this, FloatingFaceBubbleService::class.java))
        }
    }

    fun stopPetService() {
        stopService(Intent(this, FloatingFaceBubbleService::class.java))
    }

    override fun onResume() {
        super.onResume()
//        sensorManager!!.registerListener(listener, sensorAccel, SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onPause() {
        super.onPause()
        sensorManager!!.unregisterListener(listener)
    }

    var listener: SensorEventListener = object : SensorEventListener {
        override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}
        override fun onSensorChanged(event: SensorEvent) {
            when (event.sensor.type) {
                Sensor.TYPE_ROTATION_VECTOR -> {
                    // y, x, z
                    val rotationMatrix = FloatArray(16)
                    SensorManager.getRotationMatrixFromVector(rotationMatrix, event.values)
                    val remappedRotationMatrix = FloatArray(16)
                    SensorManager.remapCoordinateSystem(rotationMatrix, SensorManager.AXIS_X, SensorManager.AXIS_Z, remappedRotationMatrix)
                    val orientations = FloatArray(3)
                    SensorManager.getOrientation(remappedRotationMatrix, orientations)
                    Log.d("xxxassasa", Math.toDegrees(orientations[2].toDouble()).toString())
                }
            }
        }
    }
}

fun App.statusBarHeight(): Int {
    val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
    val defaultStatusBarHeight = resources.getDimensionPixelOffset(R.dimen.status_bar_height)
    return if (resourceId > 0) resources.getDimensionPixelSize(resourceId) else defaultStatusBarHeight
}