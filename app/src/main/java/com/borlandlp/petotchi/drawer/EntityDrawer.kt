package com.borlandlp.petotchi.drawer

import android.app.Service
import android.content.Context
import android.graphics.Color
import android.graphics.PixelFormat
import android.hardware.Sensor
import android.hardware.SensorManager
import android.os.Build
import android.util.Log
import android.view.Gravity
import android.view.MotionEvent
import android.view.WindowManager
import com.borlandlp.petotchi.CollisionEvent
import com.borlandlp.petotchi.CollisionEventHub
import com.borlandlp.petotchi.R
import com.borlandlp.petotchi.entity.Entity
import com.borlandlp.petotchi.entity.LivingEntity
import com.borlandlp.petotchi.world.World
import com.borlandlp.petotchi.entity.animation.AnimationService
import com.borlandlp.petotchi.view.CanvasLineView
import com.borlandlp.petotchi.view.CanvasView

class EntityDrawer (
    private val world: World,
    private var context: Context?,
    private val listener: SystemEventListener,
    private val animationService: AnimationService
) {
    private val window = context!!.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    private val entityMap = mutableMapOf<Entity, DrawData>()
    private val sensorManager: SensorManager = context!!.getSystemService(Service.SENSOR_SERVICE) as SensorManager
    private val sensorAccel: Sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR)
    private val sensorEventListener = AccelListener(listener)
    private val debugCollisions = mutableListOf<CollisionDrawable>()

    fun onCreate() {
        sensorManager.registerListener(sensorEventListener, sensorAccel, SensorManager.SENSOR_DELAY_FASTEST)
    }

    fun update() {
        updateData()
        draw()
    }

    private fun updateData() {
        world.entityController.getEntities().forEach { entity ->
            if (entityMap.containsKey(entity)) {
                entityMap[entity]!!.params.apply {
                    if (x != entity.location.x.toInt() || y != entity.location.y.toInt()) {
                        x = entity.location.x.toInt()
                        y = entity.location.y.toInt()
                        entityMap[entity]!!.isChanged = true
                    }
                }

                val currentState = EntityDrawState.getState(e = entity)
                if (entityMap[entity]!!.previousState != currentState) {
                    entityMap[entity]!!.view.anim = animationService.get(animData = currentState.animationId, entity = entity)
                    entityMap[entity]!!.previousState = currentState

                    // DEBUG
                    entityMap[entity]!!.view.color = when (currentState) {
                        EntityDrawState.DrawState.WALK_LEFT -> Color.BLACK
                        EntityDrawState.DrawState.WALK_RIGHT -> Color.BLACK
                        else -> Color.BLUE
                    }

                }
            } else {
                // Добавление новой сущности на экран
                val view = CanvasView(context!!)
                val layoutFlag = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
                } else {
                    WindowManager.LayoutParams.TYPE_PHONE
                }
                if (entity is LivingEntity) {
                    view.color = context!!.getColor(R.color.design_default_color_secondary_variant)
                }
                val params = WindowManager.LayoutParams(
                    entity.width,
                    entity.height,
                    layoutFlag,
                    WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN or WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT
                ).apply {
                    gravity = Gravity.TOP or Gravity.START
                    x = entity.location.x.toInt()
                    y = entity.location.y.toInt()
                }
                entityMap[entity] = DrawData(
                    view = view,
                    params = params,
                    isChanged = true,
                    previousState = EntityDrawState.getState(entity)
                )

                // TODO Если добавим спрайты длоя бордеров - исправить это
                if (entity is LivingEntity) {
                    view.anim = animationService.get(animData = EntityDrawState.getState(entity).animationId, entity = entity)
                }

                view.setOnTouchListener { v, event ->
                    v.performClick()
                    when (event.action) {
                        MotionEvent.ACTION_DOWN -> {
                            listener.onEntityTouchDown(entity, event)
                        }
                        MotionEvent.ACTION_UP -> {
                            listener.onEntityTouchUp(entity, event)
                        }
                        MotionEvent.ACTION_MOVE -> {
                            listener.onEntityMove(entity, event)
                        }
                    }
                    false
                }
                window.addView(view, params)
            }
        }

        if (CollisionEventHub.hasEvents()) {
            while (CollisionEventHub.hasEvents()) {
                val event = CollisionEventHub.get()!!

                if (debugCollisions.any { it.event.from == event.from && it.event.to == event.to && it.event.type == event.type }) {
                    return
                }

//                val view = CanvasLineView(context!!).apply {
//                    fromLoc = event.from.getCenterLocation()
//                    toLoc = event.to.getCenterLocation()
//                    color = if (event.type == CollisionType.DOWN) {
//                        context.getColor(android.R.color.holo_orange_dark)
//                    } else if (event.type == CollisionType.LEFT) {
//                        context.getColor(R.color.design_default_color_primary_variant)
//                    } else if (event.type == CollisionType.RIGHT) {
//                        context.getColor(R.color.design_default_color_secondary)
//                    } else {
//                        context.getColor(android.R.color.holo_red_light)
//                    }
//                }
//                val layoutFlag = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
//                } else {
//                    WindowManager.LayoutParams.TYPE_PHONE
//                }
//                val params = WindowManager.LayoutParams(
//                    WindowManager.LayoutParams.MATCH_PARENT,
//                    WindowManager.LayoutParams.MATCH_PARENT,
//                    layoutFlag,
//                    WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN or WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
//                    PixelFormat.TRANSLUCENT
//                )
//                debugCollisions.add(
//                    CollisionDrawable(
//                        event = event,
//                        view = view,
//                        startShowTime = System.currentTimeMillis(),
//                        displayed = true
//                    )
//                )
//                window.addView(view, params)
            }
        }
    }

    private fun draw() {
        entityMap.values.forEach {
            if (it.isChanged) {
                it.isChanged = false
//                anim.update(it.view)
                window.updateViewLayout(it.view, it.params)
            }
        }

        debugCollisions.forEach {
            if (it.displayed && it.startShowTime + 5000 < System.currentTimeMillis()) {
                if (it.view.isAttachedToWindow) {
                    window.removeView(it.view)
                }
                it.expired = true
                Log.d("xxxxxaaaa", "remove collision event")
            }
        }

        debugCollisions.filter { !it.expired }
    }

    fun onDestroy() {
        sensorManager.unregisterListener(sensorEventListener)
        entityMap.values.forEach {
            window.removeView(it.view)
        }
        context = null
    }
}

private data class CollisionDrawable(
    val event: CollisionEvent,
    val view: CanvasLineView,
    val startShowTime: Long,
    var displayed: Boolean = false,
    var expired: Boolean = false
)

private data class DrawData(
    val view: CanvasView,
    val params: WindowManager.LayoutParams,
    var isChanged: Boolean, /* true - will be redraw */
    var previousState: EntityDrawState.DrawState
)