package com.borlandlp.petotchi.drawer

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.util.Log

class AccelListener(systemListener: SystemEventListener) : SensorEventListener {
    var valuesAccel = FloatArray(3)
    var valuesAccelMotion = FloatArray(3)
    var valuesAccelGravity = FloatArray(3)

    override fun onSensorChanged(event: SensorEvent?) {
        when (event!!.sensor.type) {
            Sensor.TYPE_ROTATION_VECTOR -> {
                // y, x, z
                val rotationMatrix = FloatArray(16)
                SensorManager.getRotationMatrixFromVector(rotationMatrix, event.values)
                val remappedRotationMatrix = FloatArray(16)
                SensorManager.remapCoordinateSystem(
                    rotationMatrix,
                    SensorManager.AXIS_X,
                    SensorManager.AXIS_Z,
                    remappedRotationMatrix
                )
                val orientations = FloatArray(3)
                SensorManager.getOrientation(remappedRotationMatrix, orientations)
                Log.d("xxxassasa", Math.toDegrees(orientations[2].toDouble()).toString())
            }
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

    }
}