package com.borlandlp.petotchi.drawer

import com.borlandlp.petotchi.entity.Entity
import com.borlandlp.petotchi.entity.LivingEntity
import com.borlandlp.petotchi.entity.animation.AnimationId
import com.borlandlp.petotchi.world.DirectionList

class EntityDrawState {
    companion object {
        fun getState(e: Entity): DrawState {
            val isMoving = e is LivingEntity && (e.velocity.y != 0F || e.velocity.x != 0F)
            val isMoveLeft = isMoving && e.faceDirection == DirectionList.LEFT
            val isMoveRight = isMoving && e.faceDirection == DirectionList.RIGHT
            val isFalling = e.isFalling
            val isJumping = e is LivingEntity && e.isJumping
            val isSitting = e is LivingEntity && e.isSitting

            return when {
                isMoveLeft && !isFalling && !isJumping -> DrawState.WALK_LEFT
                isMoveRight && !isFalling && !isJumping -> DrawState.WALK_RIGHT
                isFalling && isMoveRight -> DrawState.FALLING_RIGHT
                isFalling && isMoveLeft -> DrawState.FALLING_LEFT
                isJumping && isMoveLeft -> DrawState.JUMP_LEFT
                isJumping && isMoveRight -> DrawState.JUMP_RIGHT
                isSitting && e.faceDirection == DirectionList.RIGHT -> DrawState.SITTING_RIGHT
                isSitting && e.faceDirection == DirectionList.LEFT -> DrawState.SITTING_LEFT
                else -> DrawState.STAND
            }
        }
    }

    enum class DrawState(val animationId: AnimationId) {
        WALK_LEFT(AnimationId.WALK_LEFT),
        WALK_RIGHT(AnimationId.WALK_RIGHT),
        JUMP_LEFT(AnimationId.WALK_RIGHT),
        JUMP_RIGHT(AnimationId.WALK_RIGHT),
        STAND(AnimationId.WALK_RIGHT),
        FALLING_LEFT(AnimationId.WALK_RIGHT),
        FALLING_RIGHT(AnimationId.WALK_RIGHT),
        SITTING_LEFT(AnimationId.SIT_LEFT),
        SITTING_RIGHT(AnimationId.SIT_RIGHT);
    }
}