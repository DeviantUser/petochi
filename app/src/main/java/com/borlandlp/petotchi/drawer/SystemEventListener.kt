package com.borlandlp.petotchi.drawer

import android.util.Log
import android.view.MotionEvent
import com.borlandlp.petotchi.entity.Entity
import com.borlandlp.petotchi.entity.EntityMoveByTapEvent
import com.borlandlp.petotchi.entity.EntityTapEvent
import com.borlandlp.petotchi.world.Location

class SystemEventListener {
    private var touchStartPoint = mutableMapOf<Entity, Location>()
    private var moveStartPoint = mutableMapOf<Entity, Location>()

    /* Отпустил */
    fun onEntityTouchUp(entity: Entity, event: MotionEvent) {
        if (!entity.isTouchable) {
            return
        }
        touchStartPoint.remove(entity)
        moveStartPoint.remove(entity)
        entity.isTouched = false
        Log.d(
            "xxxxxaaaa",
            "up:event${event} entity:${entity}"
        )
    }

    /* Нажал */
    fun onEntityTouchDown(entity: Entity, event: MotionEvent) {
        if (!entity.isTouchable) {
            return
        }
        val newLocation = Location(x = event.rawX, y = event.rawY)
        val behaviorEvent = EntityTapEvent(entity, newLocation)
        entity.behaviorDirector?.onEntityEvent(behaviorEvent)
        if (!behaviorEvent.isCanceled) {
            touchStartPoint[entity] = entity.location
            moveStartPoint[entity] = newLocation
            entity.isTouched = true

            Log.d(
                "xxxxxaaaa",
                "down:event${event} entity:${entity}"
            )
        }
    }

    fun onEntityMove(entity: Entity, event: MotionEvent) {
        if (!entity.isTouchable) {
            return
        }
        val newLocation = Location(
            touchStartPoint[entity]!!.x + (event.rawX - moveStartPoint[entity]!!.x).toInt(),
            touchStartPoint[entity]!!.y + (event.rawY - moveStartPoint[entity]!!.y).toInt()
        )
        val behaviorEvent = EntityMoveByTapEvent(entity, newLocation)
        entity.behaviorDirector?.onEntityEvent(behaviorEvent)
        if (!behaviorEvent.isCanceled) {
            entity.location = newLocation
            Log.d(
                "xxxxxaaaa",
                "location:${newLocation} move:event${event} entity:${entity}"
            )
        }
    }
}