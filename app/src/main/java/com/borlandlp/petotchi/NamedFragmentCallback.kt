package com.borlandlp.petotchi

import android.content.Context

interface NamedFragmentCallback {
    val isRootScreen: Boolean
        get() = false

    fun canShowToolbarShadow(): Boolean {
        return false
    }

    fun canShowBottomAppBar(): Boolean {
        return true
    }

    fun hasNotEndedActions(): Boolean {
        return false
    }

    fun needHideKeyboard(): Boolean {
        return true
    }

    fun notifyBackPressed() {}
    fun getFragmentName(context: Context?): String?
    fun getAnalyticFragmentName(context: Context?): String?
    /*val bottomAppBarTab: BottomNavButton?
        get() = BottomNavButton.PROFILE

    val actionBarContent: ToolbarContent?
        get() = ToolbarContent.TITLE*/

    fun onResumeFragment() {}
}
