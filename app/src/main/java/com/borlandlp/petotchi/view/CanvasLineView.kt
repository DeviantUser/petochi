package com.borlandlp.petotchi.view

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.os.Build
import android.util.AttributeSet
import android.view.View
import com.borlandlp.petotchi.R
import com.borlandlp.petotchi.world.Location
import com.borlandlp.petotchi.entity.animation.Animation

class CanvasLineView : View {
    private var mNumberPaint: Paint? = null
    var anim: Animation? = null
    var color = context.getColor(R.color.purple_200)
    var fromLoc: Location? = null
    var toLoc: Location? = null

    @JvmOverloads
    constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context, attrs)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context, attrs)
    }

    fun init(context: Context, attrs: AttributeSet?) {
        initPaints()
        initTabs()
    }

    private fun initPaints() {
        mNumberPaint = Paint()
        mNumberPaint!!.isAntiAlias = true
        mNumberPaint!!.style = Paint.Style.FILL
        mNumberPaint!!.strokeWidth = 10F
        mNumberPaint!!.color = context.resources.getColor(R.color.black)
    }

    private fun initTabs() {

    }

    fun start() {
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        if (anim == null) {
            mNumberPaint!!.color = color
            canvas.drawLine(fromLoc!!.x, fromLoc!!.y, toLoc!!.x, toLoc!!.y, mNumberPaint!!)
        } else {
            anim!!.update(canvas)
        }
    }
}