package com.borlandlp.petotchi.view

import android.annotation.TargetApi
import android.content.Context
import android.graphics.*
import android.os.Build
import android.util.AttributeSet
import android.view.View
import androidx.core.view.ViewCompat
import com.borlandlp.petotchi.R
import com.borlandlp.petotchi.entity.animation.Animation

class CanvasView : View, Runnable {
    private var mNumberPaint: Paint? = null
    var anim: Animation? = null
    var color = context.getColor(R.color.design_default_color_primary)

    @JvmOverloads
    constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context, attrs)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context, attrs)
    }

    fun init(context: Context, attrs: AttributeSet?) {
        initPaints()
        initTabs()
    }

    private fun initPaints() {
        mNumberPaint = Paint()
        mNumberPaint!!.isAntiAlias = true
        mNumberPaint!!.style = Paint.Style.FILL_AND_STROKE
        mNumberPaint!!.color = context.resources.getColor(R.color.black)
    }

    private fun initTabs() {

    }

    fun start() {
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
//        if (anim == null) {
            canvas.drawRect(0F, 0F, width.toFloat(), height.toFloat(), mNumberPaint!!)
            mNumberPaint!!.color = color
//        } else {
//            anim!!.update(canvas)
//        }

        ViewCompat.postOnAnimationDelayed(this, this, 100)
    }

    override fun run() {
        invalidate()
    }

    override fun performClick(): Boolean {
        super.performClick()
        return true
    }
}