package com.borlandlp.petotchi.nav

import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.NavHost

class MyNavHostFragment() : Fragment(), NavHost{
    override fun getNavController(): NavController {
        return NavController(context!!)
    }
}