package com.borlandlp.petotchi

import com.borlandlp.petotchi.entity.Entity
import java.util.*

class CollisionEventHub {
    companion object {
        var enabled = false
        private var eventHub: Queue<CollisionEvent> = LinkedList()

        fun get(): CollisionEvent? = eventHub.poll()

        fun hasEvents() = eventHub.size > 0

        fun add(event: CollisionEvent) {
            if (enabled) {
                eventHub.offer(event)
            }
        }
    }
}

data class CollisionEvent (val from: Entity, val to: Entity, val type: CollisionType)

enum class CollisionType {
    DOWN,
    LEFT,
    RIGHT,
    UP
}