package com.borlandlp.petotchi.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Matrix
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.DisplayMetrics
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream


class BitmapHelper {
    companion object {
        fun getResizedBitmap(bitmap: Bitmap, multiplier: Float): Bitmap {
            val width = bitmap.width
            val height = bitmap.height
            val matrix = Matrix()
            matrix.postScale(multiplier, multiplier)
            return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, false)
        }

        fun drawableToBitmap(drawable: Drawable): Bitmap {
            if (drawable is BitmapDrawable) {
                return drawable.bitmap
            }
            val createBitmap = Bitmap.createBitmap(
                drawable.intrinsicWidth,
                drawable.intrinsicHeight,
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(createBitmap)
            drawable.setBounds(0, 0, canvas.width, canvas.height)
            drawable.draw(canvas)
            return createBitmap
        }

        fun bitmapToDrawable(b: Bitmap, c: Context): Drawable {
            return BitmapDrawable(c.resources, b)
        }

        fun getResizedBitmap(bitmap: Bitmap, i: Int, i2: Int): Bitmap {
            val width = bitmap.width
            val height = bitmap.height
            val f = i.toFloat() / width.toFloat()
            val f2 = i2.toFloat() / height.toFloat()
            val matrix = Matrix()
            if (f > f2) {
                matrix.postScale(f, f)
            } else {
                matrix.postScale(f2, f2)
            }
            return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, false)
        }

        fun bitmapToByteArray(bitmap: Bitmap): ByteArray {
            val byteArrayOutputStream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
            return byteArrayOutputStream.toByteArray()
        }

        fun byteArrayToBitmap(bArr: ByteArray): Bitmap {
            return BitmapFactory.decodeStream(ByteArrayInputStream(bArr))
        }

        fun flip(d: BitmapDrawable): BitmapDrawable {
            val m = Matrix()
            m.preScale(-1f, 1f)
            val src = d.bitmap
            val dst = Bitmap.createBitmap(src, 0, 0, src.width, src.height, m, false)
            dst.density = DisplayMetrics.DENSITY_DEFAULT
            return BitmapDrawable(dst)
        }
    }
}