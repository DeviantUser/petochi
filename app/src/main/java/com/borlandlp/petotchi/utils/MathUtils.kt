package com.borlandlp.petotchi.utils

import com.borlandlp.petotchi.world.Location

class MathUtils {
    companion object {
        fun calcRotationAngleInDegrees(centerLoc: Location, targetLoc: Location): Double {
            // calculate the angle theta from the deltaY and deltaX values
            // (atan2 returns radians values from [-PI,PI])
            // 0 currently points EAST.
            // NOTE: By preserving Y and X param order to atan2,  we are expecting
            // a CLOCKWISE angle direction.
            var theta = Math.atan2((targetLoc.y - centerLoc.y).toDouble(), (targetLoc.x - centerLoc.x).toDouble())

            // rotate the theta angle clockwise by 90 degrees
            // (this makes 0 point NORTH)
            // NOTE: adding to an angle rotates it clockwise.
            // subtracting would rotate it counter-clockwise
            theta += Math.PI / 2.0

            // convert from radians to degrees
            // this will give you an angle from [0->270],[-180,0]
            var angle = Math.toDegrees(theta)

            // convert to positive range [0-360)
            // since we want to prevent negative angles, adjust them now.
            // we can assume that atan2 will not return a negative value
            // greater than one partial rotation
            if (angle < 0) {
                angle += 360.0
            }
            return angle
        }
    }
}